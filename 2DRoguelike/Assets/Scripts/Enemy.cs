﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MovingObject {

	public int playerDmg;


	private Animator animator;
	private Transform target;
	private bool skipMove;
	private bool onCrystal;




	protected override void Start () {
		GameManager.instance.addEnemytoList (this);
		animator = GetComponent<Animator> ();
		target = GameObject.FindGameObjectWithTag ("Player").transform;
		base.Start ();
	}

	protected override void attemptMove <T> (int xDir, int yDir) {
		if (skipMove) { 
			if(!onCrystal)
				skipMove = false;
			return;
		}
		base.attemptMove <T> (xDir, yDir);

		if(gameObject.CompareTag("Enemy2"))
			skipMove = true;

		if (onCrystal)
			skipMove = true;
	}

	public void MoveEnemy() {
		int xDir = 0;
		int yDir = 0;

		if (Mathf.Abs (target.position.x - transform.position.x) < float.Epsilon)
			yDir = target.position.y > transform.position.y ? 1 : -1;
		else
			xDir = target.position.x > transform.position.x ? 1 : -1;
		attemptMove<Player> (xDir, yDir);
	}

	protected override void OnCantMove <T> (T component) {
		Player hitPlayer = component as Player;
		hitPlayer.LoseFood (playerDmg);
		animator.SetTrigger ("EnemyAttack");
	}

	private void OnTriggerEnter2D(Collider2D other) {
		if (other.tag == "Trap")
			onCrystal = true;
	}
}
