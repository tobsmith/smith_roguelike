﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Player : MovingObject {

	public int wallDamage = 1;
	public int pointsPerFood = 10;
	public int pointsPerSoda = 20;
	public float restartLevelDelay = 1f;
	public Text foodText;

	private int columns;
	private int rows;

	private Animator animator;
	private int food;
	private List<Vector3> possibleTp = new List<Vector3> ();
	private Rigidbody2D rb2DChild;
	private BoxCollider2D boxCollide;


	// Use this for initialization
	protected override void Start () {

		animator = GetComponent<Animator> ();

		food = GameManager.instance.playerFoodPoints;

		boxCollide = GetComponent<BoxCollider2D> ();

		foodText.text = "Food: " + food;

		columns = 8;
		rows = 8;

		teleporterLocations ();

		base.Start ();

		rb2DChild = GetComponent<Rigidbody2D> ();

	}

	private void OnDisable() {

		GameManager.instance.playerFoodPoints = food;

	}

	protected override void attemptMove <T> (int xDir, int yDir) {
		if (GameManager.instance.skipTurn) {
			//print ("Skipping turn");
			goto End;
		}
		food--;
		foodText.text = "Food: " + food;
		base.attemptMove <T> (xDir, yDir);

		//RaycastHit2D hit;

		CheckIfGameOver ();
		goto End;
		End:
		//print ("End");
		GameManager.instance.playersTurn = false;
		GameManager.instance.skipTurn = false;


	}


	private void CheckIfGameOver() {

		if (food <=0) 
			GameManager.instance.gameOver();
		

	}



	// Update is called once per frame
	void Update () {
		if (!GameManager.instance.playersTurn)
			return;

		int horizontal = 0;
		int verticle = 0;

		horizontal = (int)Input.GetAxisRaw ("Horizontal");
		verticle = (int)Input.GetAxisRaw ("Vertical");

		if (horizontal != 0)
			verticle = 0;
		if (horizontal != 0 || verticle != 0)
			attemptMove<Wall> (horizontal, verticle);
	}


	protected override void OnCantMove <T> (T component) {

		Wall hitWall = component as Wall;
		hitWall.DamageWall (wallDamage);
		animator.SetTrigger ("PlayerChop");
	}

	private void Restart() {

		SceneManager.LoadScene (0);

	}

	public void LoseFood (int loss) {
		animator.SetTrigger ("PlayerHit");
		food -= loss;
		foodText.text = "-" + loss + " Food: " + food; 
		CheckIfGameOver ();
	}

	private void teleporterLocations () {
		int i, j;
		for(i = 0; i < columns; i++) {
			for(j = 0; j < rows; j++) {
				if (i == 0 || i == (columns - 1) || j == 0 || j == (rows - 1)) {
					possibleTp.Add(new Vector3 (i, j, 0f));
					//print (i + " " + j);
				}
				/*if (i == (columns - 1) && j == (rows - 1))
					possibleTp.Remove (new Vector3(i, j, 0f));*/
			}
		}

	}

	private void OnTriggerEnter2D (Collider2D other) {
		//bool alreadyTeleporting;
		if (other.tag == "Exit") {
			Invoke ("Restart", restartLevelDelay);
			enabled = false;
		} else if (other.tag == "Food") {
			food += pointsPerFood;
			foodText.text = "+" + pointsPerFood + " Food: " + food;
			other.gameObject.SetActive (false);
		} else if (other.tag == "Soda") {
			food += pointsPerSoda;
			foodText.text = "+" + pointsPerSoda + " Food: " + food;
			other.gameObject.SetActive (false);
		} else if (other.tag == "Teleporter") {
			
			int randomIndex = Random.Range (1, possibleTp.Count);
			//print (randomIndex);
			//print (possibleTp.Count);
			rb2DChild.MovePosition (new Vector2 ((int)possibleTp [randomIndex].x, (int)possibleTp [randomIndex].y));
			//print ((int)possibleTp [randomIndex].x + " " + (int)possibleTp [randomIndex].y);
			other.gameObject.SetActive (false);
		} else if (other.tag == "Trap") {
			//print ("hit red");
			GameManager.instance.skipTurn = true;
			other.gameObject.SetActive (false);
		} else if (other.tag == "Invincible") {
			foreach (Enemy enemy  in GameManager.instance.enemies) {
				enemy.gameObject.SetActive (false);
			}
			GameManager.instance.enemies.Clear ();
			other.gameObject.SetActive (false);
		}
	}
}
