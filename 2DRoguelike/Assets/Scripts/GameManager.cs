﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

	public float levelStartDelay = 2f;
	public float turnDelay = .1f;
	public static GameManager instance = null;             
	private BoardManager boardScript;                       
	private int level = 1;                                  
	public int playerFoodPoints = 100;
	[HideInInspector] public bool playersTurn = true;
	[HideInInspector] public bool skipTurn = false;
	float timer = 40;
	public Text TimerText;

	[HideInInspector]
	public List<Enemy> enemies;
	public bool enemiesMoving;
	private Text levelText;
	private GameObject levelImage;
	private bool doingSetup;

	void Awake()
	{
		
		if (instance == null)

			instance = this;

		else if (instance != this)
			Destroy(gameObject);    


		DontDestroyOnLoad(gameObject);

		enemies = new List<Enemy> ();

		boardScript = GetComponent<BoardManager>();

		InitGame();
	}

	void OnLevelWasLoaded(int index) {
		level++;
		if ((level % 5) == 0)
			timer += 40;
		InitGame ();
	}

	/*void OnEnable() {
		SceneManager.sceneLoaded += OnLevelFinishedLoading;
	}

	void OnDisable() {
		SceneManager.sceneLoaded -= OnLevelFinishedLoading;

	}*/

	public void gameOver() {
		levelText.text = "After " + level + " days, you starved.";
		levelImage.SetActive (true);
		enabled = false;
	}

	void InitGame()
	{
		doingSetup = true;

		TimerText = GameObject.Find ("TimerText").GetComponent<Text>();
		TimerText.text = "Timer: " + timer;
		levelImage = GameObject.Find("LevelImage");
		levelText = GameObject.Find ("LevelText").GetComponent<Text>();
		levelText.text = "Day " + level;
		levelImage.SetActive (true);
		Invoke ("HideLevelImage", levelStartDelay);


		enemies.Clear ();
		boardScript.SetupScene(level);

	}

	private void HideLevelImage() {
		levelImage.SetActive (false);
		doingSetup = false;
	}

	public void addEnemytoList(Enemy script) {
		enemies.Add (script);

	}
		

	//Update is called every frame.
	void Update()
	{
		timer -= Time.deltaTime;
		TimerText.text = "Timer :" + timer;
		if (timer <= 0)
			gameOver ();
		//if (skipTurn) goto Enemymove;
		if (playersTurn || enemiesMoving || doingSetup)
			return;
		/*goto Enemymove;
		Enemymove:*/
		StartCoroutine (MoveEnemies ());
	}

	IEnumerator MoveEnemies() {

		enemiesMoving = true;
		if (enemies.Count == 0) {
			yield return new WaitForSeconds (turnDelay);
		}
		for (int i = 0; i < enemies.Count; i++) {
			enemies[i].MoveEnemy();
			yield return new WaitForSeconds (enemies[i].moveTime);
		}

		playersTurn = true;

		enemiesMoving = false;
	}
}
